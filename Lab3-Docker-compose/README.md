# How to build and run this Docker-compose

Clone this branch in a new directory which I'll refer to as **CLONE_DIRECTORY**.
Go to IntelliJ and type within the terminal:

    mvn package spring-boot:repackage -Dpostgre_hostname="localhost" -Dmongo_hostname="localhost" -Dserver_address="0.0.0.0" -Dserver_port="8080"
    
    cp target/Lab3-0.0.1-SNAPSHOT.jar <CLONE_DIRECTORY>/files/config-lab3.jar
    
Then in a new console go to <CLONE_DIRECTORY> and run:

    sudo docker build --tag=openjdk10:base --rm=true .
    
    sudo docker volume create --name=app_internet-lab3
    
    docker-compose config
    
    docker-compose build
    
    docker-compose up