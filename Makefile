clean_all: 
	make -B clean_lab 
	make -B clean_postgres 
	make -B clean_mongo
	
clean_compose:
	cd Lab3-Docker-compose;\
	docker-compose down
	@docker rm lab3-gruppo6
	@docker rm postgres-lab3-compose
	@docker rm mongo-lab3-compose

clean_lab:
	@docker stop lab3-gruppo6
	@docker rm lab3-gruppo6
clean_postgres:
	@docker stop postgres-lab3-gruppo6
	@docker rm postgres-lab3-gruppo6
clean_mongo:
	@docker stop mongo-lab3-gruppo6
	@docker rm mongo-lab3-gruppo6 

docker_all: 
	make -B docker_postgres 
	make -B docker_Lab3

docker_Lab3:
	mvn package -Dmaven.test.skip=true
	cp target/Lab3-0.0.1-SNAPSHOT.jar docker_lab;\
	cd docker_lab;\
	docker build --build-arg JAR_FILE=Lab3-0.0.1-SNAPSHOT.jar -t gruppo6/lab3:latest .;\
	cd ..

docker_postgres:
	cd docker_postgres;\
	docker build -t gruppo6/postgresql:latest .;\
	cd ..

run_all: run_postgres run_mongo run_Lab3

run_postgres:
	docker run --name postgres-lab3-gruppo6 -p 5432:5432 -d -i -t gruppo6/postgresql
run_mongo:
	docker run --name mongo-lab3-gruppo6 -p 27017:27017 -d -i -t -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin -e MONGO_INITDB_DATABASE=Lab3 mongo
run_Lab3:
	docker run --name lab3-gruppo6 -t -i -d -p 8080:8080 --link postgres-lab3-gruppo6:postgres-lab3-gruppo6 --link mongo-lab3-gruppo6:mongo-lab3-gruppo6 gruppo6/lab3

build_run: docker_all run_all

rebuild_lab: clean_lab docker_Lab3 run_Lab3
start:
	docker start postgres-lab3-gruppo6
	docker start mongo-lab3-gruppo6
	docker start lab3-gruppo6

compose:
	cd Lab3-Docker-compose;\
	docker-compose build	
compose-build:
	mvn package spring-boot:repackage -Dmaven.test.skip=true -Dpostgre_hostname="localhost" -Dmongo_hostname="localhost" -Dserver_address="0.0.0.0" -Dserver_port="8080"
	
	cp target/Lab3-1.0.0-RELEASE.jar Lab3-Docker-compose/files/config-lab3.jar
	make compose
compose-start:
	cd Lab3-Docker-compose;\
	docker-compose up -d
compose-build-and-start:
	cd Lab3-Docker-compose;\
	docker-compose up -d --build --force-recreate

