package it.polito.appInternet.Lab3.configuration.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("it.polito.appInternet.Lab3.repository.mongo")
public class MongoConfiguration extends AbstractMongoConfiguration {

    private final static String configurationFilePath = "";
    private final int port = 27017;
    private final String mongoUser = "admin";
    private final String mongoPwd = "admin";
    private final String mongoAuthDb = "admin";
    private final String mongoDbName = "Lab3";
    @Value("${mongo_hostname}")
    private String mongo_hostname;

    /*  TODO verificare se è possibile creare un costruttore dove inizializzare
        i parametri di configurazione del database
     */

    @Override
    @Bean
    public MongoClient mongoClient() {
/*
        MongoConfigurationParameters config = null;

        try {
            InputStream fileStream = this.getClass().getResourceAsStream(configurationFilePath);

            ObjectMapper objectMapper = new ObjectMapper();
            config = objectMapper.readValue(fileStream, MongoConfigurationParameters.class);

        }  catch (JsonParseException | JsonMappingException jse) {
            throw new RuntimeException();
        } catch (IOException e) {
            throw new RuntimeException();
        }

        MongoCredential credential = MongoCredential.createCredential(config.getMongoAuthDb(), config.getMongoDbName(), config.getMongoPwd().toCharArray());
        ServerAddress mongoAddress = new ServerAddress(config.getMongoHostname(), config.getMongoPort());
        return new MongoClient(mongoAddress,
                credential,
                MongoClientOptions.builder().build());*/
        MongoCredential credential = MongoCredential.createCredential(mongoUser, mongoAuthDb, mongoPwd.toCharArray());
        ServerAddress mongoAddress = new ServerAddress(mongo_hostname, port);
        return new MongoClient(mongoAddress, credential, MongoClientOptions.builder().build());
    }

    @Override
    protected String getDatabaseName() {
        return mongoDbName;
    }

}
