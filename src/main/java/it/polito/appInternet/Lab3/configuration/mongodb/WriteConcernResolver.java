package it.polito.appInternet.Lab3.configuration.mongodb;


import com.mongodb.WriteConcern;
import org.springframework.data.mongodb.core.MongoAction;

public interface WriteConcernResolver {

    WriteConcern resolve(MongoAction action);
}


