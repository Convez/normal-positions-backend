package it.polito.appInternet.Lab3.configuration.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JwtUtils {

    private final TokenStore tokenStore;

    @Autowired
    public JwtUtils(TokenStore tokenStore) {

        this.tokenStore = tokenStore;
    }

    public String getUsernameFromToken(String accessToken) {

        String authS = accessToken.split(" ")[1];
        OAuth2Authentication auth = tokenStore.readAuthentication(authS);
        Map<String, Object> details = (Map<String, Object>) auth.getDetails();
        if (details.containsKey("user_name")) return (String) details.get("user_name");

        return "";
    }

    public String getRoleFromToken(String accessToken) {

        OAuth2Authentication auth = tokenStore.readAuthentication(accessToken.replace("Bearer ", ""));
        Map<String, Object> details = (Map<String, Object>) auth.getDetails();
        if (details.containsKey("authorities")) return (String) details.get("authorities");

        return "";
    }

    public String getScopeFromToken(String accessToken) {

        OAuth2Authentication auth = tokenStore.readAuthentication(accessToken.replace("Bearer ", ""));
        Map<String, Object> details = (Map<String, Object>) auth.getDetails();
        if (details.containsKey("scope")) return (String) details.get("scope");
        return "";
    }

}
