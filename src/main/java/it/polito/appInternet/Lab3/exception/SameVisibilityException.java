package it.polito.appInternet.Lab3.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED, reason = "Same visibility already set")
public class SameVisibilityException extends RuntimeException{
}
