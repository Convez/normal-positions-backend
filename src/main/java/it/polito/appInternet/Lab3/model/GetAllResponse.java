package it.polito.appInternet.Lab3.model;

import it.polito.appInternet.Lab3.model.position_rest.Positions;

import java.util.List;

public class GetAllResponse {

    private List<PositionsArchive> uploaded;

    private List<PositionsArchive> bought;

    public GetAllResponse(List<PositionsArchive> uploaded, List<PositionsArchive> bought) {
        this.uploaded = uploaded;
        this.bought = bought;
    }

    public List<PositionsArchive> getUploaded() {
        return uploaded;
    }

    public void setUploaded(List<PositionsArchive> uploaded) {
        this.uploaded = uploaded;
    }

    public List<PositionsArchive> getBought() {
        return bought;
    }

    public void setBought(List<PositionsArchive> bought) {
        this.bought = bought;
    }
}
