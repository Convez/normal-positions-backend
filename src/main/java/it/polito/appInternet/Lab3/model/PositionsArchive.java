package it.polito.appInternet.Lab3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Document(collection = "archives")
public class PositionsArchive {

    @Id
    private ObjectId id;

    private String stringId;

    @Indexed
    private String userName;

    private Long timesBought;

    private Double totalProfit;

    private Double archivePrice;

    private Boolean searchable;

    private List<GeoJsonPoint> approxPositions;

    private List<Long> approxTimestamps;

    private List<UserPosition> actualPositions;

    public PositionsArchive(){
    }
    public PositionsArchive(String userName, Long timesBought, Double totalProfit, Double archivePrice, Boolean searchable) {
        this.userName = userName;
        this.timesBought = timesBought;
        this.totalProfit = totalProfit;
        this.archivePrice = archivePrice;
        this.searchable = searchable;
    }
    public PositionsArchive(String userName, Long timesBought, Double totalProfit, Double archivePrice, Boolean searchable, Boolean owned, List<GeoJsonPoint> approxPositions, List<Long> approxTimestamps) {
        this.userName = userName;
        this.timesBought = timesBought;
        this.totalProfit = totalProfit;
        this.archivePrice = archivePrice;
        this.searchable = searchable;
        this.approxPositions = approxPositions;
        this.approxTimestamps = approxTimestamps;
    }

    public static Stream<Long> generateApproximatedTimestamps(Stream<Long> originalTimestamps){
        // Generate Archive approximated representations

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        // Hackerman
        return originalTimestamps.map(t-> {
            try {
                return df.parse(df.format(new Date(t*1000))).getTime()/1000;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(Objects::nonNull).distinct();
    }
    public static Stream<GeoJsonPoint> generateApproximatedPositions(Stream<GeoJsonPoint> originalPositions){
        return originalPositions
                .map(d -> {
                    Double x = Math.round(d.getX() * 100.0) / 100.0;
                    Double y = Math.round(d.getY() * 100.0) / 100.0;
                    return new GeoJsonPoint(x,y);
                })
                .distinct();
    }
    public String getId() {
        return id.toHexString();
    }
    @JsonIgnore
    public ObjectId getObjectId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTimesBought() {
        return timesBought;
    }

    public void setTimesBought(Long timesBought) {
        this.timesBought = timesBought;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Double getArchivePrice() {
        return archivePrice;
    }

    public void setArchivePrice(Double archivePrice) {
        this.archivePrice = archivePrice;
    }


    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    public List<GeoJsonPoint> getApproxPositions() {
        return approxPositions;
    }

    public void setApproxPositions(List<GeoJsonPoint> approxPositions) {
        this.approxPositions = approxPositions;
    }

    public List<Long> getApproxTimestamps() {
        return approxTimestamps;
    }

    public void setApproxTimestamps(List<Long> approxTimestamps) {
        this.approxTimestamps = approxTimestamps;
    }

    public List<UserPosition> getActualPositions() {
        return actualPositions;
    }

    public void setActualPositions(List<UserPosition> actualPositions) {
        this.actualPositions = actualPositions;
    }
}
