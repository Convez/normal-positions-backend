package it.polito.appInternet.Lab3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.polito.appInternet.Lab3.configuration.mongodb.GeoJsonLineStringSerializer;
import it.polito.appInternet.Lab3.configuration.mongodb.GeoJsonPointSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Objects;

@Document(collection = "positions")
public class UserPosition {

    @Id
    @JsonIgnore
    private ObjectId id;

    @Indexed
    private ObjectId archiveId;

    @Indexed
    private String userName;

    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    //@JsonSerialize(using = GeoJsonPointSerializer.class)
    private GeoJsonPoint geoData;

    @Min(value = 0)
    @NotNull
    private Long timestamp;

    @Min(value = 0)
    private Double cost;

    private Boolean searchable;

    public UserPosition() {
    }

    public UserPosition(ObjectId id, String userName, GeoJsonPoint geoData, Long timestamp, Boolean searchable) {

        this.id = id;
        this.userName = userName;
        this.geoData = geoData;
        this.timestamp = timestamp;
        this.cost=0.5;
        this.searchable = searchable;
    }

    public String getId() {
        return id.toHexString();
    }
    @JsonIgnore
    public ObjectId getObjectId() {
        return id;
    }

    public void setId(ObjectId _id) {
        this.id = _id;
    }

    public String getArchiveId() {
        return archiveId.toHexString();
    }
    @JsonIgnore
    public ObjectId getArchiveObjectId(){return archiveId;}

    public void setArchiveId(ObjectId archiveId) {
        this.archiveId = archiveId;
    }
    public void setArchiveId(String archiveId) {
        this.archiveId = new ObjectId(archiveId);
    }

    public String getUserName() {

        return userName;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public GeoJsonPoint getGeoData() {

        return geoData;
    }

    public void setGeoData(GeoJsonPoint geoData) {

        this.geoData = geoData;
    }

    public Long getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Long timestamp) {

        this.timestamp = timestamp;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPosition that = (UserPosition) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(geoData, that.geoData) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userName, geoData, timestamp);
    }

    @Override
    public String toString() {

        return "UserPosition{" + "userName='" + userName + '\'' + ", geoData=" + geoData + ", timestamp=" + timestamp + '}';
    }
}
