package it.polito.appInternet.Lab3.model.position_rest;

public class MongoConfigurationParameters {

    private String mongoDbName;
    private String mongoAuthDb;
    private String mongoHostname;
    private int mongoPort;
    private String mongoUsername;
    private String mongoPwd;

    public String getMongoDbName() {

        return mongoDbName;
    }

    public void setMongoDbName(String mongoDbName) {

        this.mongoDbName = mongoDbName;
    }

    public String getMongoHostname() {

        return mongoHostname;
    }

    public void setMongoHostname(String mongoHostname) {

        this.mongoHostname = mongoHostname;
    }

    public int getMongoPort() {

        return mongoPort;
    }

    public void setMongoPort(int mongoPort) {

        this.mongoPort = mongoPort;
    }

    public String getMongoUsername() {

        return mongoUsername;
    }

    public void setMongoUsername(String mongoUsername) {

        this.mongoUsername = mongoUsername;
    }

    public String getMongoPwd() {

        return mongoPwd;
    }

    public void setMongoPwd(String mongoPwd) {

        this.mongoPwd = mongoPwd;
    }

    public String getMongoAuthDb() {

        return mongoAuthDb;
    }

    public void setMongoAuthDb(String mongoAuthDb) {

        this.mongoAuthDb = mongoAuthDb;
    }
}
