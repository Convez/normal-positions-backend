package it.polito.appInternet.Lab3.model.position_rest;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.polito.appInternet.Lab3.configuration.mongodb.GeoJsonLineStringSerializer;
import org.springframework.data.mongodb.core.geo.GeoJsonLineString;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.List;

public class Positions extends ResourceSupport {

    @NotNull
    private List<Long> timestamp;

    @JsonSerialize(using = GeoJsonLineStringSerializer.class)
    //@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonLineString jsonPosition;

    public List<Long> getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(List<Long> timestamp) {

        this.timestamp = timestamp;
    }

    public GeoJsonLineString getJsonPosition() {

        return jsonPosition;
    }

    public void setJsonPosition(GeoJsonLineString jsonPosition) {

        this.jsonPosition = jsonPosition;
    }
}

