package it.polito.appInternet.Lab3.model.position_rest;

import org.springframework.hateoas.ResourceSupport;

public class SinglePosition extends ResourceSupport {

    private double latitude;

    private double longitude;

    private long timestamp;

    public SinglePosition(double latitude, double longitude, long timestamp) {

        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    public double getLatitude() {

        return latitude;
    }

    public void setLatitude(double latitude) {

        this.latitude = latitude;
    }

    public double getLongitude() {

        return longitude;
    }

    public void setLongitude(double longitude) {

        this.longitude = longitude;
    }

    public long getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(long timestamp) {

        this.timestamp = timestamp;
    }
}
