package it.polito.appInternet.Lab3.model.security;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "AUTHORITY", uniqueConstraints = {@UniqueConstraint(columnNames = {"NAME"})})
public class Authority implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Authority() {

    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof Authority)) return false;
        Authority authority = (Authority) o;
        return Objects.equals(getName(), authority.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }

    public String getName() {


        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public String getAuthority() {
        return getName();
    }
}
