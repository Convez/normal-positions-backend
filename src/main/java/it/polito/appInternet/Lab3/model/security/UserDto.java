package it.polito.appInternet.Lab3.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.polito.appInternet.Lab3.configuration.util.ValidPassword;
import it.polito.appInternet.Lab3.configuration.util.ValidUsername;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ValidPassword
public class    UserDto {

    @ValidUsername
    @NotNull
    @NotEmpty
    @JsonProperty("username")
    private String username;

    @NotNull
    @NotEmpty
    @JsonProperty("password")
    private String password;

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }
}
