package it.polito.appInternet.Lab3.repository.jpa;

import it.polito.appInternet.Lab3.model.security.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JPAAuthorityRepository extends JpaRepository<Authority, Long> {

    @Query("SELECT DISTINCT authority FROM Authority authority " + "WHERE authority.name = :name")
    Authority findByName(@Param("name") String name);

}
