package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.bank.BankTransaction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

public interface BankTransactionRepository extends MongoRepository<BankTransaction, String> {

    Optional<BankTransaction> findById(ObjectId id);
    Collection<BankTransaction> findAllByUserName(String userName);
    Collection<BankTransaction> findAllByUserNameAndFromName(String userName, String fromName);
}
