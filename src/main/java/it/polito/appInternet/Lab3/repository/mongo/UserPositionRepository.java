package it.polito.appInternet.Lab3.repository.mongo;

import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.model.security.User;
import org.bson.types.ObjectId;
import org.springframework.data.geo.Polygon;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.print.attribute.standard.JobKOctets;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

@Repository
public interface UserPositionRepository extends MongoRepository<UserPosition, String> {

    Stream<UserPosition> findAllByIdIn(List<ObjectId> ids);
    Stream<UserPosition> findAllByArchiveIdIn(List<ObjectId> archiveIds);
    Stream<UserPosition> findUserPositionByArchiveId(ObjectId archiveId);
    Stream<UserPosition> findUserPositionsByUserName(String userName);
    Stream<UserPosition> findUserPositionsByArchiveId(ObjectId archiveId);
    HashSet<UserPosition> findUserPositionsByUserNameIsAndGeoDataIsInAndTimestampIsIn(
            String userName,
            List<GeoJsonPoint> geoJsonPoints,
            List<Long> timeStamps
    );

    Stream<UserPosition> findUserPositionsByUserNameAndGeoDataWithinAndTimestampBetween(String userName, Polygon p, Long after, Long before);
    Stream<UserPosition> findUserPositionsByGeoDataIsWithinAndTimestampBetween(Polygon p, Long after, Long before);
    Stream<UserPosition> findUserPositionsByGeoDataIsWithinAndTimestampBetweenAndSearchable(Polygon p, Long after, Long before,Boolean searchable);
    Stream<UserPosition> findUserPositionsByUserNameIsNotAndGeoDataIsWithinAndTimestampBetweenAndSearchable(String userName,Polygon p, Long after, Long before,Boolean searchable);
    Long deleteAllByIdIn(List<ObjectId> ids);
    Long deleteUserPositionByUserName(String userName);
    Long deleteUserPositionByArchiveIdAndUserName(ObjectId archiveId, String userName);
}
