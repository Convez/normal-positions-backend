package it.polito.appInternet.Lab3.rest;

import it.polito.appInternet.Lab3.model.bank.BankAccount;
import it.polito.appInternet.Lab3.model.bank.BankTransaction;
import it.polito.appInternet.Lab3.model.security.User;
import it.polito.appInternet.Lab3.repository.mongo.BankAccountRepository;
import it.polito.appInternet.Lab3.repository.jpa.JPAUserRepository;
import it.polito.appInternet.Lab3.repository.mongo.UserPositionRepository;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.configuration.util.JwtUtils;
import it.polito.appInternet.Lab3.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@CrossOrigin
@RestController
@RequestMapping("/secured/admin")
@PreAuthorize("hasRole('ADMIN')")
public class AdminPositionController {

    private final UserPositionRepository userPositionRepository;
    private final BankAccountRepository bankAccountRepository;
    private final it.polito.appInternet.Lab3.repository.jpa.JPAUserRepository JPAUserRepository;
    private final JwtUtils jwtUtils;
    private final BankAccountService accountService;
    @Autowired
    public AdminPositionController(UserPositionRepository userPositionRepository, JwtUtils jwtUtils, BankAccountService accountService, JPAUserRepository JPAUserRepository, BankAccountRepository bankAccountRepository) {
        this.userPositionRepository = userPositionRepository;
        this.jwtUtils = jwtUtils;
        this.accountService = accountService;
        this.JPAUserRepository = JPAUserRepository;
        this.bankAccountRepository = bankAccountRepository;
    }
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public List<User> getUserList(){
        // In our project each user will have only one role, so that I just get the first element from the Authority collection
        return JPAUserRepository.getAllUsers();

    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{userName}/positions",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public Stream<UserPosition> getUserPositionsForUser(@PathVariable("userName") String userName){
        // In our project each user will have only one role, so that I just get the first element from the Authority collection
        return userPositionRepository.findUserPositionsByUserName(userName);

    }
    
//    @RequestMapping(
//            method = RequestMethod.GET,
//            value = "/customer/{userName}/positions",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    @ResponseBody
//    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('ADMIN')")
//    public Collection<BankTransaction> getCustomerPositionsForCustomer(@PathVariable("userName") String userName){
//
//        BankAccount accountRequested = bankAccountRepository.findBankAccountByUserName(userName);
//        if(accountRequested != null)
//            return accountRequested.getAccountHistory();
//        else
//            return null;
//    }
}
