package it.polito.appInternet.Lab3.rest;


import com.sun.istack.NotNull;
import it.polito.appInternet.Lab3.configuration.util.JwtUtils;
import it.polito.appInternet.Lab3.model.PolygonSearchRequest;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.model.bank.BankTransaction;
import it.polito.appInternet.Lab3.service.ArchivePersistenceService;
import it.polito.appInternet.Lab3.service.BankAccountService;
import it.polito.appInternet.Lab3.service.CustomerPositionService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@CrossOrigin
@RestController
@RequestMapping("/secured/customer/archive")
public class CustomerArchiveREST {


    private final ArchivePersistenceService archivePersistenceService;
    private final CustomerPositionService customerPositionService;
    private final BankAccountService bankAccountService;
    private final JwtUtils jwtUtils;

    @Autowired
    public CustomerArchiveREST(ArchivePersistenceService archivePersistenceService, CustomerPositionService customerPositionService, BankAccountService bankAccountService, JwtUtils jwtUtils) {
        this.archivePersistenceService = archivePersistenceService;
        this.customerPositionService = customerPositionService;
        this.bankAccountService = bankAccountService;
        this.jwtUtils = jwtUtils;
    }

    @RequestMapping(
            value = "/search",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public Stream<PositionsArchive> getArchivesWithPositionsWithinPolygon(
            @RequestBody PolygonSearchRequest polygonSearchRequest,
            @RequestHeader(value="Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return customerPositionService.getPositionInsidePolygon(userName, polygonSearchRequest);
    }
    @RequestMapping(
            value = "/search/check",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public Map<String,Boolean> checkArchivesAlreadyPurchased(
            @RequestBody List<ObjectId> archivesToCheck,
            @RequestHeader(value = "Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return bankAccountService.checkArchivesAreOwned(userName,archivesToCheck);
    }
    @RequestMapping(
            value = "/purchase",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
@PreAuthorize("hasRole('USER')")
    public Stream<PositionsArchive> purchaseArchives(
            @Valid @RequestBody @NotNull List<ObjectId> archivesToPurchase,
            BindingResult result,
            @RequestHeader(value = "Authorization") String accessToken
    ){

        //Check if the list of ids is a valid list
        if(result.hasErrors())
            throw new ValidationException();
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        List<PositionsArchive> toPurchase = archivePersistenceService
                .getArchivesFromIds(archivesToPurchase);
        return bankAccountService.buyArchives(userName,toPurchase);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/history",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
@PreAuthorize("hasRole('USER')")
    public Collection<BankTransaction> getTransactionsForCustomer(
            @RequestHeader(value="Authorization") String accessToken
    ){
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return bankAccountService.getHistory(userName);
    }

    @RequestMapping(
            value = "/transaction/{tid}/archive/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public Stream<UserPosition> getTransactionSingleArchive (
            @PathVariable("id") ObjectId archiveId,
            @PathVariable("tid") ObjectId transactionId,
            @RequestHeader(value = "Authorization") String accessToken
    ){
        System.out.println(archiveId);
        String userName = jwtUtils.getUsernameFromToken(accessToken);
        return bankAccountService.getTransactionArchivePositions(userName,transactionId,archiveId);
    }
}
