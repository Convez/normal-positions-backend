package it.polito.appInternet.Lab3.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OauthAccessController {

    private static String authorizationRequestBaseUri = "oauth/authorize";
    private final ClientRegistrationRepository clientRegistrationRepository;
    private Map<String, String> oauth2AuthenticationUrls = new HashMap<>();

    @Autowired
    public OauthAccessController(ClientRegistrationRepository clientRegistrationRepository) {

        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    @GetMapping("/oauth_access")
    public String oauthAccess(Model model) {

        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository).as(Iterable.class);
        if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }

        clientRegistrations.forEach(registration -> oauth2AuthenticationUrls.put(registration.getClientName(), authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
        model.addAttribute("urls", oauth2AuthenticationUrls);

        return "oauth_access";

    }
}
