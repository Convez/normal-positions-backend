package it.polito.appInternet.Lab3.rest;

import it.polito.appInternet.Lab3.model.security.UserDto;
import it.polito.appInternet.Lab3.service.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

@Controller
public class SignUpController {

    private final SignUpService service;

    @Autowired
    public SignUpController(SignUpService service) {
        this.service = service;
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public ResponseEntity registerUserAccount(
            @RequestBody @Valid UserDto accountDto,
            BindingResult result, WebRequest request, Errors errors) {

        if (!result.hasErrors()) {
            createUserAccount(accountDto);
        } else
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    private void createUserAccount(UserDto accountDto) {
        service.registerNewUserAccount(accountDto);
    }
}
