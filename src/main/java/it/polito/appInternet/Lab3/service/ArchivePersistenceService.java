package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.stream.Stream;


public interface ArchivePersistenceService {
    Boolean isAlreadyUploaded(String userName, List<UserPosition> potentialArchive);
    void createAndSaveArchiveFrom(String userName, List<UserPosition> positions);

    Stream<PositionsArchive> retrieveUserUploadedArchives(String userName);

    Stream<PositionsArchive> retrieveFilledUploadedArchives(String userName);

    Stream<PositionsArchive> retrieveFilledBoughtArchives(String userName);

    void setArchiveSearchVisibility(String userName, ObjectId archiveId, Boolean newVisibility);

    List<PositionsArchive> getArchivesFromIds(List<ObjectId> ids);

    Stream<UserPosition> retrieveArchiveDetails(String userName, ObjectId archiveId);
}