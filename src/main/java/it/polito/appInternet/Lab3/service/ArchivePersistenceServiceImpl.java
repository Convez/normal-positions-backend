package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.exception.SameVisibilityException;
import it.polito.appInternet.Lab3.exception.NotFoundException;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.repository.mongo.BankTransactionRepository;
import it.polito.appInternet.Lab3.repository.mongo.PositionsArchiveRepository;
import it.polito.appInternet.Lab3.repository.mongo.UserPositionRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ArchivePersistenceServiceImpl implements ArchivePersistenceService {

    private final PositionsArchiveRepository positionsArchiveRepository;
    private final UserPositionRepository userPositionRepository;
    private final BankTransactionRepository bankTransactionRepository;

    public ArchivePersistenceServiceImpl(PositionsArchiveRepository positionsArchiveRepository, UserPositionRepository userPositionRepository, BankTransactionRepository bankTransactionRepository) {
        this.positionsArchiveRepository = positionsArchiveRepository;
        this.userPositionRepository = userPositionRepository;
        this.bankTransactionRepository = bankTransactionRepository;
    }
    public Boolean isAlreadyUploaded(String userName, List<UserPosition> potentialArchive){

        List<GeoJsonPoint> geoJsonPoints = potentialArchive.stream().map(UserPosition::getGeoData).collect(Collectors.toList());
        List<Long> timeStamps = potentialArchive.stream().map(UserPosition::getTimestamp).collect(Collectors.toList());

        //TODO Decide if it is worth keeping or too much computing power in the long run
        //Check if the user has already uploaded the same set of positions
        HashSet<UserPosition> alreadyUploadedPositions = userPositionRepository
                .findUserPositionsByUserNameIsAndGeoDataIsInAndTimestampIsIn(
                        userName,
                        geoJsonPoints,
                        timeStamps
                );

        return alreadyUploadedPositions.containsAll(potentialArchive);
    }

    public void createAndSaveArchiveFrom(String userName, List<UserPosition> positions){
        Double totalCost = positions.size() * 0.5;
        PositionsArchive positionsArchive = new PositionsArchive(
                userName,
                0L,
                0.0,
                totalCost,
                true);
        positionsArchiveRepository.save(positionsArchive);
        positions.forEach(p->p.setArchiveId(positionsArchive.getId()));
        userPositionRepository.saveAll(positions);
    }

    public Stream<PositionsArchive> retrieveUserUploadedArchives(String userName){
        return positionsArchiveRepository.findPositionsArchivesByUserName(userName);
    }

    @Override
    public Stream<PositionsArchive> retrieveFilledUploadedArchives(String userName){
        Map<ObjectId, PositionsArchive> archives = retrieveUserUploadedArchives(userName)
                .collect(Collectors.toMap(
                        PositionsArchive::getObjectId,
                        p->p
                ));
        Map<ObjectId, List<UserPosition>> positions = userPositionRepository
                .findAllByArchiveIdIn(new LinkedList<>(archives.keySet()))
                .collect(
                        Collectors.groupingBy(UserPosition::getObjectId)
                );
        return archives.values().stream()
                .peek(
                        a->{
                           a.setActualPositions(
                                   positions.get(a.getObjectId())
                           );
                        }
                );
    }
    @Override
    public Stream<PositionsArchive> retrieveFilledBoughtArchives(String userName) {
        List<ObjectId> archives = bankTransactionRepository
                .findAllByUserNameAndFromName(userName,userName)
                .stream().flatMap(
                        p->p.getArchiveIds().keySet().stream()
                )
                .map(ObjectId::new).collect(Collectors.toList());
        Stream<PositionsArchive> archiveStream = positionsArchiveRepository
                .findPositionsArchivesByIdIn(archives)
                .peek(ar->{
                    ar.setTimesBought(null);
                    ar.setTotalProfit(null);
                    ar.setSearchable(null);
                });
        Map<ObjectId,List<UserPosition>> positions = userPositionRepository
                .findAllByArchiveIdIn(archives)
                .peek(p->{
                    p.setSearchable(null);
                })
                .collect(Collectors.groupingBy(
                        UserPosition::getObjectId
                ));
        return archiveStream.peek(
                p->p.setActualPositions(positions.get(p.getObjectId()))
        );
    }
    public void setArchiveSearchVisibility(String userName, ObjectId archiveId, Boolean newVisibility){
        System.out.println(archiveId);
        Optional<PositionsArchive> archive = positionsArchiveRepository.findPositionsArchiveById(archiveId);
        if(!archive.isPresent())
            throw new NotFoundException();
        if(!archive.get().getUserName().equals(userName))
            throw new UnauthorizedUserException("Archive not uploaded by user");
        PositionsArchive positionsArchive = archive.get();
        if(positionsArchive.getSearchable().equals(newVisibility))
            throw new SameVisibilityException();
        positionsArchive.setSearchable(newVisibility);
        List<UserPosition> archivePositions = userPositionRepository
                .findUserPositionsByArchiveId(archiveId)
                .peek(p->p.setSearchable(newVisibility))
                .collect(Collectors.toList());
        positionsArchiveRepository.save(positionsArchive);
        userPositionRepository.saveAll(archivePositions);
    }

    @Override
    public List<PositionsArchive> getArchivesFromIds(List<ObjectId> ids){
        return positionsArchiveRepository
                .findPositionsArchivesByIdIn(ids).collect(Collectors.toList());

    }

    @Override
    public Stream<UserPosition> retrieveArchiveDetails(String userName, ObjectId archiveId){
        Optional<PositionsArchive> archive = positionsArchiveRepository.findPositionsArchiveById(archiveId);
        if(archive.isPresent() && archive.get().getUserName().equals(userName)){
            return userPositionRepository.findUserPositionsByArchiveId(archiveId);
        }
        throw new UnauthorizedUserException("Archive not uploaded by user");
    }
}
