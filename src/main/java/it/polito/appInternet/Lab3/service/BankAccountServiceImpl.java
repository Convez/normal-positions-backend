package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.exception.CustomerIsPoorException;
import it.polito.appInternet.Lab3.exception.NotFoundException;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.model.bank.BankAccount;
import it.polito.appInternet.Lab3.model.bank.BankTransaction;
import it.polito.appInternet.Lab3.repository.mongo.BankAccountRepository;
import it.polito.appInternet.Lab3.repository.mongo.BankTransactionRepository;
import it.polito.appInternet.Lab3.repository.mongo.PositionsArchiveRepository;
import it.polito.appInternet.Lab3.repository.mongo.UserPositionRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    private final BankAccountRepository bankAccountRepository;
    private final BankTransactionRepository bankTransactionRepository;

    private final PositionsArchiveRepository positionsArchiveRepository;
    private final UserPositionRepository userPositionRepository;

    @Autowired
    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, BankTransactionRepository bankTransactionRepository, PositionsArchiveRepository positionsArchiveRepository, UserPositionRepository userPositionRepository) {

        this.bankAccountRepository = bankAccountRepository;
        this.bankTransactionRepository = bankTransactionRepository;
        this.positionsArchiveRepository = positionsArchiveRepository;
        this.userPositionRepository = userPositionRepository;
    }

    @Override
    public Collection<BankTransaction> getHistory(String userName) {
        return bankTransactionRepository.findAllByUserNameAndFromName(userName, userName);
    }

    @Override
    public Map<String,Boolean> checkArchivesAreOwned(String userName, List<ObjectId> archivesIds){
        Collection<BankTransaction> history = getHistory(userName);

        List<String> s = history.stream()
                .filter(h->h.getArchiveIds()!=null)
                .flatMap(h->h.getArchiveIds().keySet().stream())
                .collect(Collectors.toList());
        return archivesIds.stream()
                .map(ObjectId::toHexString)
                .collect(Collectors.toMap(
                        archive->archive,
                        s::contains
                ));
    }
    @Override
    @Transactional
    public Stream<PositionsArchive> buyArchives(String userName, Collection<PositionsArchive> archives) {
        BankAccount userAccount = bankAccountRepository.findBankAccountByUserName(userName);
        if(userAccount == null){
            BankAccount newuserAccount = new BankAccount();
            newuserAccount.setAmount(0.0);
            newuserAccount.setAccountHistory(new LinkedList<>());
            newuserAccount.setUserName(userName);
            bankAccountRepository.save(newuserAccount);
            throw new CustomerIsPoorException();
        }
        System.out.println(userAccount);
        Double totCost = archives.stream().mapToDouble(PositionsArchive::getArchivePrice).sum();
        if(userAccount.getAmount()<totCost){
            throw new CustomerIsPoorException();
        }
        archives.stream().collect(Collectors.groupingBy(PositionsArchive::getUserName))
                .forEach((key, value) -> {
                    BankAccount beneficiary = bankAccountRepository
                            .findBankAccountByUserName(
                                    key
                            );
                    Double totCredit = value.stream()
                            .peek(positionsArchive -> {
                                positionsArchive.setTotalProfit(positionsArchive.getTotalProfit()+positionsArchive.getArchivePrice());
                                positionsArchive.setTimesBought(positionsArchive.getTimesBought()+1);
                            })
                            .mapToDouble(PositionsArchive::getArchivePrice).sum();
                    BankTransaction beneficiaryTransaction = new BankTransaction();
                    beneficiaryTransaction.setAmount(totCredit);
                    beneficiaryTransaction.setFromName(userAccount.getUserName());
                    beneficiaryTransaction.setUserName(key);
                    beneficiaryTransaction.setArchiveIds(
                            value.stream()
                                    .collect(Collectors.toMap(
                                            PositionsArchive::getId,
                                            PositionsArchive::getUserName
                                    )));
                    bankTransactionRepository.save(beneficiaryTransaction);
                    beneficiary.addTransaction(beneficiaryTransaction.getId());
                    bankAccountRepository.save(beneficiary);
                });

        BankTransaction transaction = new BankTransaction();
        transaction.setAmount(-totCost);
        transaction.setFromName(userAccount.getUserName());
        transaction.setUserName(userAccount.getUserName());
        transaction.setArchiveIds(
                archives
                        .stream()
                        .collect(Collectors.toMap(
                                PositionsArchive::getId,
                                PositionsArchive::getUserName
                        ))
        );
        bankTransactionRepository.save(transaction);
        userAccount.addTransaction(transaction.getId());
        bankAccountRepository.save(userAccount);
        positionsArchiveRepository.saveAll(archives);
        Map<ObjectId, List<UserPosition>> justBought = userPositionRepository
                .findAllByArchiveIdIn(archives.stream().map(PositionsArchive::getObjectId).collect(Collectors.toList()))
                .collect(Collectors.groupingBy(UserPosition::getArchiveObjectId));
        return archives.stream()
                .peek(p->{
                    p.setSearchable(null);
                    p.setTotalProfit(null);
                    p.setTimesBought(null);
                    p.setActualPositions(justBought.get(p.getObjectId()));
                });
    }

    @Override
    public Stream<UserPosition> getTransactionArchivePositions(String userName, ObjectId transactionId, ObjectId archiveId){
        Optional<BankTransaction> transaction = bankTransactionRepository.findById(transactionId);
        if(! transaction.isPresent())
            throw new NotFoundException();
        BankTransaction bankTransaction =transaction.get();
        if(bankTransaction.getUserName().equals(userName) &&
                bankTransaction.getArchiveIds().keySet().contains(archiveId.toHexString())){
            return userPositionRepository.findUserPositionsByArchiveId(archiveId);
        }
        throw new UnauthorizedUserException("User did not buy archive");
    }

}
