package it.polito.appInternet.Lab3.service;

import com.mongodb.MongoQueryException;
import it.polito.appInternet.Lab3.exception.NotFoundException;
import it.polito.appInternet.Lab3.model.PolygonSearchRequest;
import it.polito.appInternet.Lab3.model.PositionsArchive;
import it.polito.appInternet.Lab3.model.UserPosition;
import it.polito.appInternet.Lab3.repository.mongo.PositionsArchiveRepository;
import it.polito.appInternet.Lab3.repository.mongo.UserPositionRepository;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.UncategorizedMongoDbException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CustomerPositionServiceImpl implements CustomerPositionService {

    private Logger logger = Logger.getLogger(CustomerPositionServiceImpl.class);

    private final UserPositionRepository userPositionRepository;

    private final PositionsArchiveRepository positionsArchiveRepository;

    @Autowired
    public CustomerPositionServiceImpl(UserPositionRepository userPositionRepository, PositionsArchiveRepository positionsArchiveRepository) {
        this.userPositionRepository = userPositionRepository;
        this.positionsArchiveRepository = positionsArchiveRepository;
    }

    @Override
    public Stream<PositionsArchive> getPositionInsidePolygon(String userName, PolygonSearchRequest polygonSearchRequest) {

        try {
            Map<String, List<UserPosition>> userPositionsMap = userPositionRepository
                    .findUserPositionsByUserNameIsNotAndGeoDataIsWithinAndTimestampBetweenAndSearchable(userName,
                            polygonSearchRequest.getInnerPolygon(), polygonSearchRequest.getAfter(),
                            polygonSearchRequest.getBefore(), true)
                    .collect(Collectors.groupingBy(UserPosition::getArchiveId));

            return positionsArchiveRepository
                    .findPositionsArchivesByIdIn(userPositionsMap.keySet())
                    .peek(positionsArchive -> {
                        positionsArchive.setTimesBought(null);
                        positionsArchive.setTotalProfit(null);
                        positionsArchive.setApproxPositions(
                                PositionsArchive.generateApproximatedPositions(
                                        userPositionsMap.get(positionsArchive.getId())
                                                .stream()
                                                .map(UserPosition::getGeoData)
                                ).collect(Collectors.toList())
                        );
                        positionsArchive.setApproxTimestamps(
                                PositionsArchive.generateApproximatedTimestamps(
                                        userPositionsMap.get(
                                                positionsArchive.getId()
                                        ).stream()
                                                .map(UserPosition::getTimestamp)
                                ).collect(Collectors.toList())
                        );
                    });
        } catch (UncategorizedMongoDbException e) {
            if(e.getCause() instanceof MongoQueryException)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Latitude/Longitude is out of bounds!");
            else
                throw e;
        }
    }
}
