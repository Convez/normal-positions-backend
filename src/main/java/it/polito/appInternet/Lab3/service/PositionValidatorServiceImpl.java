package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.model.UserPosition;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PositionValidatorServiceImpl implements PositionValidatorService {

    private Logger logger = Logger.getLogger(PositionValidatorServiceImpl.class);

    /*
        check if the position list satisfy the time and distance constraint
     */

    @Override
    public boolean validatePositionList(List<UserPosition> positions) {

        // check if the positions are between -90 and +90 and -180 and 180
        for (int i = 0; i < positions.size(); i++) {
            String error_string = "";
            boolean error = false;

            if (positions.get(i).getGeoData().getX() < -90.00) {
                error_string = "the latitude is less than -90.00";
                error = true;
            }
            if (positions.get(i).getGeoData().getX() > 90.00) {
                error_string = "the latitude is grater than 90.00";
                error = true;
            }
            if (positions.get(i).getGeoData().getY() < -180.00) {
                error_string = "the latitude is less than 180.00";
                error = true;
            }
            if (positions.get(i).getGeoData().getY() > 180.00) {
                error_string = "the latitude is greater than 180.00";
                error = true;
            }

            if (error) {
                logger.error("Error: the point at position [" + i + "]," + error_string);
                return false;
            }
        }

        // check if the speed is feasible
        for (int i = 1; i < positions.size(); i++) {

            if (positions.get(i - 1).getTimestamp() == null) {
                logger.error("position timestamp not valid: " + positions.get(i).getTimestamp() + " " + positions.get(i - 1).getTimestamp());
                return false;
            }

            if (positions.get(i).getTimestamp() < positions.get(i - 1).getTimestamp()) {
                logger.error("position timestamp not valid: " + positions.get(i).getTimestamp() + " " + positions.get(i - 1).getTimestamp());
                return false;
            }

            // get the time interval between the actual position and the last position
            Long deltaT = positions.get(i).getTimestamp() - positions.get(i - 1).getTimestamp();
            double dis;

            // compute the speed
            if ((dis = distance(positions.get(i).getGeoData().getX(),
                    positions.get(i).getGeoData().getY(),
                    positions.get(i - 1).getGeoData().getX(),
                    positions.get(i - 1).getGeoData().getY())) / deltaT > 100) {
                logger.error("the distance cover between two geographical point is: " + dis);
                logger.error("the speed is not feasible" + dis / deltaT);
                return false;
            }
        }

        logger.info("Validated positively");
        return true;
    }

    @Override
    public List<UserPosition> validateAndMergePosition(List<UserPosition> DbPositions, List<UserPosition> requestPositions) {

        if (DbPositions == null || DbPositions.isEmpty()) return null;

        List<UserPosition> mergedListPos = new ArrayList<>(DbPositions);
        mergedListPos.addAll(requestPositions);

        if (validatePositionList(mergedListPos)) {
            return mergedListPos;
        } else {
            return null;
        }

    }

    /*@Override
    public boolean validateMergePositionList(Positions submittetPositions, Positions persistedPositions) {

        persistedPositions.getJsonPosition().getCoordinates().addAll(submittetPositions.getJsonPosition().getCoordinates());
        persistedPositions.getTimestamp().addAll(persistedPositions.getTimestamp());

        return validatePositionList(persistedPositions);
    }*/

    /*
        compute the distance between two points
     */
    private double distance(double lat1, double lon1, double lat2, double lon2) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        // Convert to Kilometers to meters
        dist = dist * 1.609344 * 1000;
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts decimal degrees to radians						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts radians to decimal degrees						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {

        return (rad * 180 / Math.PI);
    }

}
