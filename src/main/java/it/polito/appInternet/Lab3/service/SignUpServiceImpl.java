package it.polito.appInternet.Lab3.service;

import it.polito.appInternet.Lab3.exception.UserAlreadyExistException;
import it.polito.appInternet.Lab3.model.security.Authority;
import it.polito.appInternet.Lab3.model.security.User;
import it.polito.appInternet.Lab3.model.security.UserDto;
import it.polito.appInternet.Lab3.repository.jpa.JPAAuthorityRepository;
import it.polito.appInternet.Lab3.repository.jpa.JPAUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedList;

@Service
@Transactional
public class SignUpServiceImpl implements SignUpService {

    private final JPAUserRepository jpaUserRepository;

    private final JPAAuthorityRepository jpaAuthorityRepository;

    @Autowired
    public SignUpServiceImpl(JPAUserRepository jpaUserRepository, JPAAuthorityRepository jpaAuthorityRepository) {
        this.jpaUserRepository = jpaUserRepository;
        this.jpaAuthorityRepository = jpaAuthorityRepository;
    }

    @Override
    public void registerNewUserAccount(UserDto userDto) {
        if(userExist(userDto.getUsername()))
            throw new UserAlreadyExistException("Username not available, select another username!");

        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt(8)));
        user.setAccountExpired(false);
        user.setAccountLocked(false);
        user.setCredentialsExpired(false);
        user.setEnabled(true);

        Collection<Authority> authorities = new LinkedList<>();
        Authority authority = jpaAuthorityRepository.findByName("ROLE_USER");

        if(authority == null) {
            throw new RuntimeException();
        }

        authorities.add(authority);
        user.setAuthorities(authorities);

        jpaUserRepository.save(user);
    }

    @Override
    public boolean userExist(String username) {
        return jpaUserRepository.findByUsername(username) != null;
    }
}
