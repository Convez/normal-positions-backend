package it.polito.appInternet.Lab3;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.util.JacksonJsonParser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.LinkedList;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Lab3Application.class)
@WebAppConfiguration
public class AdminActionsTest {

    @Autowired
    private WebApplicationContext wac;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private FilterChainProxy filterChainProxy;

    private MockMvc mockMvc;
    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(filterChainProxy).build();
    }
    private String obtainAccessToken(String username, String password) throws Exception {
        ResultActions result
                = mockMvc.perform(post("/oauth/token")
                .content(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("grant_type","password")
                .param("client_id","app-internet-oauth2-read-client")
                .param("username","davide")
                .param("password","admin")
                .with(httpBasic("app-internet-oauth2-read-client","AppInternet_ReadClient"))
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Test
    public void testMvcLoaded(){
        Assert.notNull(mockMvc,"Could not load web app context");
    }

    @Test
    public void testGetAdminToken(){
        try {
            String accessToken = obtainAccessToken("davide","admin");
            Assert.hasLength(accessToken,"Empty token");
        } catch (Exception e) {
            Assert.isNull(e,e.getMessage());
        }
    }
    @Test
    public void testAdminAccessCustomerApiError(){
        try {
            String accessToken= obtainAccessToken("davide", "admin");
            mockMvc.perform(get("/secured/customer")
                    .header("Authorization", "Barer "+ accessToken)
            ).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/positions")
                    .header("Authorization", "Barer "+ accessToken)
            ).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/search")
                .header("Authorization", "Barer "+ accessToken)
            ).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/buy")
                    .header("Authorization", "Barer "+ accessToken)
            ).andExpect(status().isUnauthorized());
        }catch (Exception e){
            Assert.isNull(e,e.getMessage());
        }
    }
    @Test
    public void testAccessAdminNoToken(){
        try {
            mockMvc.perform(get("/secured/admin"))
                    .andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/admin/davide/positions"))
                    .andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/admin/customer/davide/positions"))
                    .andExpect(status().isUnauthorized());
        } catch (Exception e) {
            Assert.notNull(e,e.getMessage());
        }
    }
    @Test
    public void testAdminAccessAdminApi(){
        String accessToken= null;
        try {
            accessToken = obtainAccessToken("davide", "admin");
            mockMvc.perform(
                    get("/secured/admin")
                    .header("Authorization", "Bearer "+ accessToken)
            ).andExpect(status().isNotFound());
            mockMvc.perform(
                    get("/secured/admin/davide/positions")
                            .header("Authorization", "Bearer "+ accessToken)
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            ).andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
            mockMvc.perform(
                    get("/secured/admin/customer/davide/positions")
                            .header("Authorization", "Bearer "+ accessToken)
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
            ).andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        } catch (Exception e) {
            Assert.notNull(e,e.getMessage());
        }
    }
}
