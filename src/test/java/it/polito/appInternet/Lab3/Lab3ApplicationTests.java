package it.polito.appInternet.Lab3;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.util.JacksonJsonParser;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = Lab3Application.class)
public class Lab3ApplicationTests {

    @Autowired
    private TestRest controller;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JwtTokenStore tokenStore;

    @Autowired
    private WebApplicationContext wac;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private FilterChainProxy filterChainProxy;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(filterChainProxy).build();
    }

    /**
     * Test if the controller is loaded
     */
    @Test
    public void contextLoads() {

        assertThat(controller).isNotNull();
    }

    @Test
    public void whenTokenDoesNotContainIssuer_thenSuccess() {

        String tokenValue = obtainAccessToken();
        System.out.println("TOKEN: " + tokenValue);
        OAuth2Authentication auth = tokenStore.readAuthentication(tokenValue);
        Map<String, Object> details = (Map<String, Object>) auth.getDetails();

        assertTrue(details.containsKey("authorities"));
    }

    private String obtainAccessToken() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("davide");
        resourceDetails.setPassword("admin");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        return restTemplate.getAccessToken().getValue();
    }

    private String obtainAccessToken(String username, String password) throws Exception {

        ResultActions result = mockMvc.perform(post("/oauth/token").content(MediaType.APPLICATION_FORM_URLENCODED_VALUE).param("grant_type", "password").param("client_id", "app-internet-oauth2-read-client").param("username", username).param("password", password).with(httpBasic("app-internet-oauth2-read-client", "AppInternet_ReadClient")).accept("application/json;charset=UTF-8")).andExpect(status().isOk()).andExpect(content().contentType("application/json;charset=UTF-8"));
        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }


    /**
     * Test if OAuth is working
     */
    @Test
    public void testCorrectAdminAuth() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("davide");
        resourceDetails.setPassword("admin");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/secured/test/admin", String.class);
        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        System.out.println("Message response: " + response.getBody());
    }

    @Test(expected = OAuth2AccessDeniedException.class)
    public void testUncorrectRole() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("enrico");
        resourceDetails.setPassword("user");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        restTemplate.getForEntity("http://localhost:8080/secured/test/admin", String.class);
    }

    @Test(expected = OAuth2AccessDeniedException.class)
    public void testUncorrectAuth() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("enrico");
        resourceDetails.setPassword("user2");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        restTemplate.getForEntity("http://localhost:8080/secured/test/admin", String.class);
    }

    @Test
    public void testCorrectUserAuth() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("enrico");
        resourceDetails.setPassword("user");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/secured/test/user", String.class);
        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        System.out.println("Message response: " + response.getBody());
    }

    @Test
    public void testCorrectCustomerAuth() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("alessandro");
        resourceDetails.setPassword("customer");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/secured/test/customer", String.class);
        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        System.out.println("Message response: " + response.getBody());
    }

    @Test
    public void testCorrectAuth() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("aiman");
        resourceDetails.setPassword("customer");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("app-internet-oauth2-read-client");
        resourceDetails.setClientSecret("AppInternet_ReadClient");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Collections.singletonList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/secured/test", String.class);
        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        System.out.println("Message response: " + response.getBody());
    }

    /*
        ENRICO's TESTS
     */

    @Test
    public void testMvcLoaded() {

        Assert.notNull(mockMvc, "Could not load web app context");
    }

    @Test
    public void testGetAdminToken() {

        try {
            String accessToken = obtainAccessToken("davide", "admin");
            Assert.hasLength(accessToken, "Empty token");
        } catch (Exception e) {
            Assert.isNull(e, e.getMessage());
        }
    }

    @Test
    public void testAdminAccessCustomerApiError() {

        try {
            String accessToken = obtainAccessToken("davide", "admin");
            mockMvc.perform(get("/secured/customer").header("Authorization", "Barer " + accessToken)).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/positions").header("Authorization", "Barer " + accessToken)).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/search").header("Authorization", "Barer " + accessToken)).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/customer/buy").header("Authorization", "Barer " + accessToken)).andExpect(status().isUnauthorized());
        } catch (Exception e) {
            Assert.isNull(e, e.getMessage());
        }
    }

    @Test
    public void testAccessAdminNoToken() {

        try {
            mockMvc.perform(get("/secured/admin")).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/admin/davide/positions")).andExpect(status().isUnauthorized());
            mockMvc.perform(get("/secured/admin/customer/davide/positions")).andExpect(status().isUnauthorized());
        } catch (Exception e) {
            Assert.notNull(e, e.getMessage());
        }
    }

    @Test
    public void testAdminAccessAdminApi() {

        String accessToken;
        try {
            accessToken = obtainAccessToken("davide", "admin");
            mockMvc.perform(get("/secured/admin").header("Authorization", "Bearer " + accessToken)).andExpect(status().isNotFound());
            mockMvc.perform(get("/secured/admin/davide/positions").header("Authorization", "Bearer " + accessToken).accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
            mockMvc.perform(get("/secured/admin/customer/davide/positions").header("Authorization", "Bearer " + accessToken).accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        } catch (Exception e) {
            Assert.notNull(e, e.getMessage());
        }
    }

    /*
        Alessandro's Tests
     */

    @Test
    public void testPosition() {

        try {
            String accessToken = obtainAccessToken("enrico", "user");

            Resource resource = new ClassPathResource("path.json");
            InputStream inputStream = resource.getInputStream();

            mockMvc.perform(post("/secured/positions").header("Authorization", "Bearer " + accessToken).contentType(MediaType.APPLICATION_JSON).content(inputStream.readAllBytes())).andExpect(status().isOk());
            mockMvc.perform(get("/secured/positions").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        } catch (Exception e) {
            Assert.notNull(e, e.getMessage());
        }
    }

}
